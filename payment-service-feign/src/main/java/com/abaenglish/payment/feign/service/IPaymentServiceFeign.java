package com.abaenglish.payment.feign.service;

import org.springframework.cloud.netflix.feign.FeignClient;

@FeignClient(url = "payment-service", name = "payment-service")
public interface IPaymentServiceFeign {
}
