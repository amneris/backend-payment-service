package com.abaenglish.payment.feign;

import com.abaenglish.payment.feign.service.IPaymentServiceFeign;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("!test")
@EnableFeignClients(basePackages = "com.abaenglish.payment.feign.service")
@ConditionalOnClass({IPaymentServiceFeign.class})
public class PaymentFeignAutoConfiguration {
}
